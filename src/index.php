<?php
//composer init
//composer require slim/slim:"4.*"
//composer require slim/psr7
//php -S localhost:8080 -t src src/index.php
session_start();
use Slim\Http\Response as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Comment\CommentsStorage;
use Comment\Authorization;
require __DIR__ . '/../vendor/autoload.php';

$loader = new FilesystemLoader('templates');
$view = new Environment($loader);


$config = include 'config/database.php';
$admin = include 'config/adminData.php';

try
{
    $connection = new PDO($config['path']);
    $connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch (PDOException $exception)
{
    echo 'DB error: '.$exception->getMessage();
    die();
}

$comStorage = new CommentsStorage($connection);
$auth = new Authorization($admin['login'],$admin['pass']);

$app = AppFactory::create();
$app->addBodyParsingMiddleware();


$app->get('/', function ($request, $response) use($view) {
    $body = $view->render('Hello.twig',
        ['name'=>'world!']
    );
    $response->getBody()->write($body);
    return $response;
});
$app->get('/about', function ($request, $response) use($view) {

    $body = $view->render('index.twig');

    $response->getBody()->write($body);
    return $response;
});


$app->get('/api/feedbacks/{url}', function ($request, $response,$args) use($view,$comStorage) {
    $coms = $comStorage->getById(intval($args['url']));
    //var_dump($coms);
    if(empty($coms))
    {
        $body=$view->render('Not_found.twig');
    }
    else{
        $body = $view->render('index.twig',[
            'coms'=>$coms
        ]);
    }

    // $response->getBody()->write((string)json_encode($coms));
    $response->getBody()->write($body);
    return $response;
});

$app->get('/api/feedbacks/', function ($request, $response,$args) use($app,$view,$comStorage) {
    $pageNumber =  $_GET['page'];
    $coms = $comStorage->getListByPage(intval($pageNumber));
    //$coms = json_decode($coms);
    if(empty($coms))
    {
        $body=$view->render('Not_found.twig');
    }
    else{
        $body = $view->render('index.twig',[
            'coms'=>$coms
        ]);
    }
    //var_dump($coms);
    //$response->getBody()->withJson($coms);
    $response->getBody()->write($body);
    return $response;
});

$app->get('/admin', function ($request, $response) use($view) {

    $body = $view->render('authForm.twig');
    $response->getBody()->write($body);
    return $response;
});

$app->post('/auth-post', function ($request, $response) use($view,$app,$auth) {
    $params = (array) $request->getParsedBody();
    $isAdmin = $auth->checkAdmin($params['login'], $params['password']);
    if($isAdmin===true)
    {
        $_SESSION['admin']=true;
        return $response->withStatus(302)->withHeader('Location', '/about');
    }

    return $response->withStatus(302)->withHeader('Location', '/about');
});

$app->get('/delete/{id}', function ($request, $response,$args) use($view,$comStorage) {
    if($_SESSION['admin']===true)
    {
        if(empty($args['id']))
        {
            echo 'Пустой id';
        }
        else{
            $comStorage->deleteById($args['id']);
            $response->getBody()->write('Запись с id'.$args['id'].' удалена');
        }
    }
    else{
        return $response->withStatus(302)->withHeader('Location', '/about');
    }
    return $response;
});

$app->get('/adminunset', function ($request, $response,$args) use($view,$comStorage) {
   unset($_SESSION['admin']);
   return $response->withStatus(302)->withHeader('Location', '/about');
});

$app->get('/insert', function ($request, $response) use($view) {

    $body = $view->render('insert.twig');
    $response->getBody()->write($body);
    return $response;
});

$app->post('/insert-post', function ($request, $response) use($view,$app,$comStorage) {
   // $params = (array) $request->getParsedBody();
    $arr = array(
        'name' => $_POST['name'],
	    'text' =>  $_POST['text'],
        'date'=>$_POST['date']
);
    $data=json_encode($arr);
    $comStorage->insert($data);

    return $response->withStatus(302)->withHeader('Location', '/about');
});

$app->run();