<?php

namespace Comment;
use PDO;
class CommentsStorage
{
    private PDO $connection;

    public function __construct(PDO $connection)
    {
        $this->connection=$connection;
    }

    public function getById(int $id):?array
    {
        $statement = $this->connection->prepare('SELECT * FROM comments WHERE id = :id');
        $statement->execute([
            'id'=>$id
        ]);
        $result=$statement->fetchAll();
        return $result;
    }

    public function getListByPage($page)
    {
        $idTo = intval($page*20);
        $idFrom = intval($idTo-19);

        $statement = $this->connection->prepare('SELECT * FROM comments WHERE id BETWEEN '.$idFrom.' AND '.$idTo.' ;');
        $statement->execute();
        $result=$statement->fetchAll();
       // return json_encode($result);
        return $result;
    }

    public function deleteById($id)
    {
        $statement = $this->connection->prepare('DELETE FROM comments WHERE id = :id ');
        $statement->execute(['id'=>$id]);

    }

    public function insert($data)
    {
        $arr=json_decode($data,true);
        $statement = $this->connection->prepare('INSERT INTO comments (name, text, date) VALUES (?,?,?);');
        $statement->execute([$arr['name'],$arr['text'],$arr['date']]);

    }


}