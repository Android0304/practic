<?php
namespace Comment;
class Authorization
{
    private $login;
    private $password;

    public function __construct($log,$pass)
    {
       $this->login=$log;
       $this->password=$pass;
    }
    public function checkAdmin($log,$pass):bool
    {
        if(empty($this->login)||empty($this->password))
        {
            throw new Exception("Логин и пароль пустые");
        }
        if($this->login!=$log || $this->password!=$pass)
        {
            throw new Exception("Неверный логин или пароль");
        }
        return true;
    }
}